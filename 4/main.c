// 2015110705
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>

struct subject {
	int computer_architecture;
	int operating_system;
	int data_structure;
	int data_communication;
};

struct person
{
	int no;
	char name[20];
	struct subject score;
};

void display1(struct person p);
void display2(struct person *p);
void disp_score(struct subject s);

int main()
{
	struct person p = {9618005, "홍미영", {98, 95, 97, 100}};

	printf("display1 \n");
	display1(p);

	printf("\ndisplay2 \n");
	display2(&p);

	return 0;
}


void display1(struct person p)
{
	printf("학번 : %d \n", p.no);
	printf("이름 : %s \n", p.name);
	printf("----------------------------------- \n");
	printf("과목명 \t\t 점수 \n");
	printf("----------------------------------- \n");
	disp_score(p.score);
	printf("----------------------------------- \n");
}

void display2(struct person *p)
{
	printf("학번 : %d \n", p->no);
	printf("이름 : %s \n", p->name);
	printf("----------------------------------- \n");
	printf("과목명 \t\t 점수 \n");
	printf("----------------------------------- \n");
	disp_score(p->score);
	printf("----------------------------------- \n");
}

void disp_score(struct subject s)
{
	printf("컴퓨터구조 \t %d \n", s.computer_architecture);	
	printf("운영체제 \t %d \n", s.operating_system);	
	printf("자료구조 \t %d \n", s.data_structure);	
	printf("데이터통신 \t %d \n", s.data_communication);	
}