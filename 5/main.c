// 2015110705
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

struct subject {
	int computer_architecture;
	int operating_system;
	int data_structure;
	int data_communication;
};

struct person
{
	int no;
	char name[20];
	struct subject score;
};

void input(struct person *p);
void display1(struct person *p);
void display2(struct person **p);
void disp_score(struct subject s);

int main()
{
	struct person *ptr = (struct person*)malloc(sizeof(struct person));

	input(ptr);

	printf("\ndisplay1 \n");
	display1(ptr);

	printf("\ndisplay2 \n");
	display2(&ptr);

	return 0;
}

void input(struct person *p)
{
	printf("정보를 입력하시오! \n");
	printf("학번\t\t : ");
	scanf("%d", &(p->no));
	printf("이름\t\t : ");
	scanf("%s", &(p->name));
	printf("컴퓨터구조\t : ");
	scanf("%d", &(p->score.computer_architecture));
	printf("운영체제\t : ");
	scanf("%d", &(p->score.operating_system));
	printf("자료구조\t : ");
	scanf("%d", &(p->score.data_structure));
	printf("데이터통신\t : ");
	scanf("%d", &(p->score.data_communication));
}

void display1(struct person *p)
{
	printf("학번 : %d \n", p->no);
	printf("이름 : %s \n", p->name);
	printf("----------------------------------- \n");
	printf("과목명 \t\t 점수 \n");
	printf("----------------------------------- \n");
	disp_score(p->score);
	printf("----------------------------------- \n");
}

void display2(struct person **p)
{
	printf("학번 : %d \n", (*p)->no);
	printf("이름 : %s \n", (*p)->name);
	printf("----------------------------------- \n");
	printf("과목명 \t\t 점수 \n");
	printf("----------------------------------- \n");
	disp_score((*p)->score);
	printf("----------------------------------- \n");
}

void disp_score(struct subject s)
{
	printf("컴퓨터구조 \t %d \n", s.computer_architecture);	
	printf("운영체제 \t %d \n", s.operating_system);	
	printf("자료구조 \t %d \n", s.data_structure);	
	printf("데이터통신 \t %d \n", s.data_communication);	
}