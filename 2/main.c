// 2015110705
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

#define MAX_COL 10

typedef struct {
	int col;
	int row;
	int value;
} term;

void fastTranspose1(term *a, term *b);
void fastTranspose2(term *a, term *b);
void printMatrix(term[]);

int main()
{
	term a[10], b[10];
	FILE* pfInput = fopen("a.txt", "r");
	FILE* pfOutput;
	int i;

	for (i = 0; i < MAX_COL; i++) {
		fscanf(pfInput, "%d", &a[i].row);
		fscanf(pfInput, "%d", &a[i].col);
		fscanf(pfInput, "%d", &a[i].value);
	}

	fclose(pfInput);

	//fastTranspose1(a, b);
	fastTranspose2(a, b);

	printf("A \n");
	printMatrix(a);
	printf("B \n");
	printMatrix(b);

	pfOutput = fopen("b.txt", "w");
	for (i = 0; i <= b[0].value; i++) {
		fprintf(pfInput, "%d ", b[i].row);
		fprintf(pfInput, "%d ", b[i].col);
		fprintf(pfInput, "%d \n", b[i].value);
	}
	fclose(pfOutput);
}

void fastTranspose1(term *a, term *b)
{
	int rowTerms[MAX_COL], startingPos[MAX_COL];
	int i, j, numCols = a->col, numTerms = a->value;
	b->row = numCols; b->col = a->row;
	b->value = numTerms;
	if (numTerms > 0) {
		for (i = 0; i < numCols; i++)
			rowTerms[i] = 0;
		for (i = 1; i <= numTerms; i++)
			rowTerms[(a + i)->col]++;
		startingPos[0] = 1;
		for (i = 1; i < numCols; i++)
			startingPos[i] = startingPos[i - 1] + rowTerms[i - 1];
		for (i = 1; i <= numTerms; i++) {
			j = startingPos[a[i].col]++;
			(b + j)->row = (a + i)->col; (b + j)->col = (a + i)->row;
			(b + j)->value = (a + i)->value;
		}
	}
}

void fastTranspose2(term *a, term *b)
{
	int rowTerms[MAX_COL], startingPos[MAX_COL];
	int i, j, numCols = (*a).col, numTerms = (*a).value;
	(*b).row = numCols; (*b).col = (*a).row;
	(*b).value = numTerms;
	if (numTerms > 0) {
		for (i = 0; i < numCols; i++)
			rowTerms[i] = 0;
		for (i = 1; i <= numTerms; i++)
			rowTerms[(*(a + i)).col]++;
		startingPos[0] = 1;
		for (i = 1; i < numCols; i++)
			startingPos[i] = startingPos[i - 1] + rowTerms[i - 1];
		for (i = 1; i <= numTerms; i++) {
			j = startingPos[(*(a + i)).col]++;
			(*(b + j)).row = (*(a + i)).col; (*(b + j)).col = (*(a + i)).row;
			(*(b + j)).value = (*(a + i)).value;
		}
	}
}

void printMatrix(term arr[])
{
	int i, j;
	int count = 1;

	for (i = 0; i < arr[0].row; i++) {
		for (j = 0; j < arr[0].col; j++) {
			if(arr[count].col == j && arr[count].row == i) {
				printf("%4d ", arr[count].value);
				count++;
			}
			else printf("%4d ", 0);
		}
		printf("\n");
	}
}